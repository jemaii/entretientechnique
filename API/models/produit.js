var mongoose = require('mongoose');
var Schema = mongoose.Schema;
// create a schema for Produit
var produitSchema = new Schema({
      productName: {type:String},
      basePrice: {type:String},
      category: {type:String},
      brand: {type:String},
      productMaterial: {type:String},
      imageUrl: {type:String},
      delivery: {type:Date},
      details: {type:String},
      reviews:[]

});
var Produit = mongoose.model('Produit', produitSchema);
module.exports = Produit;
