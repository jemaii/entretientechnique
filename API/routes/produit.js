var express = require('express');
var router = express.Router();
var path = require('path');
var Produit =require('../models/produit');

var Client = require('node-rest-client').Client;


// get all produits and save them
router.get('/getallproduits/saved', function(req, res, next){
  var client = new Client();
  client.get("http://internal.ats-digital.com:3066/api/products", function (data, response) {
      // parsed response body as js object
      for (var i =0; i<data.length;i++){
        Produit.create(data[i], function (err, Produit) {
            if (err) return next(err);
          });
      }
      res.json({"message":"well saved"})

  });
});
 // get all produits

 router.get('/', function(req, res, next){
     Produit.find(function(err, Produits){
         if(err){
             res.send(err);
         }
         res.json(Produits);
     });
 });

//

router.get('/tousCategories', function(req, res, next){
  let categories = [];
    Produit.find(function(err, data){
        if(err){
            res.send(err);
        }
        categories.push(data[0].category)
        for (var i =1;i<data.length;i++){
          if(categories.indexOf(data[i].category) > -1) {
            console.log("deja existe")
          }
          else{
            categories.push(data[i].category)
          }

        }
        res.json(categories)
    });
});
module.exports = router;
