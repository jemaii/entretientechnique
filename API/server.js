var express = require('express');
var path = require('path');
var mongoose = require('mongoose');
var cors = require('cors');
var bodyParser = require('body-parser');
var produit = require('./routes/produit');


var port = 5050;
var app = express();
mongoose.Promise = global.Promise;

//connection to databse
mongoose.connect('mongodb://localhost/mesproduits',{useMongoClient: true});
var conn = mongoose.connection;
conn.on('error', console.error.bind(console, 'connection error:'));
conn.once('open', function() {
  console.log("connection is established");
});
//View Engine
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.engine('html', require('ejs').renderFile);
// Set Static Folder
app.use(express.static(path.join(__dirname, '../front/src')));
// Body Parser MW
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));


app.use(function(req, res, next) {
  var allowedOrigins = ['http://localhost:4200'];
 var origin = req.headers.origin;
 if(allowedOrigins.indexOf(origin) > -1){
     res.setHeader('Access-Control-Allow-Origin', origin);
 }
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST,HEAD, OPTIONS,PUT, DELETE,PATCH');
  //res.setHeader('Access-Control-Allow-Headers', 'Content-Type,Accept', 'Authorization');
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization,application/json");
  res.setHeader('Access-Control-Allow-Credentials', true);
  next();
});
app.use('/produit',produit);


app.listen(port, function(){
    console.log('Server started on port '+port);
});
