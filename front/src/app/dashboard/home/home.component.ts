import { Component, OnInit, trigger, state, style, transition, animate } from '@angular/core';
import { Router,ActivatedRoute}from '@angular/router';
import { ProduitService } from '../services/produit/produit.service';


@Component({
    selector: 'home-cmp',
    moduleId: module.id,
    templateUrl: 'home.component.html',
    providers:[ProduitService]
})

export class HomeComponent implements OnInit{
  produits:any[]=[];
  categories:any[]=[];
  public search:string='';


constructor(private router:Router,private produitService:ProduitService ) {

}
    ngOnInit(){
      this.produitService.getallproduits().subscribe(produits => {
                  this.produits = produits;

              },
              err => {
                  console.log(err);
                  return false;
              });
      this.produitService.getallcategories().subscribe(categories => {
                  this.categories = categories;

              },
              err => {
                  console.log(err);
                  return false;
              });

    }


}
