import { Pipe, PipeTransform } from '@angular/core';
@Pipe({
 name: 'searching'
})
export class SearchPipe implements PipeTransform {
  transform(value, args: string[]){
   if (!value) return value;
   if (value.filter((item) => item.category.toLowerCase().includes(args)) != '') {
     return value.filter((item) => item.category.toLowerCase().includes(args));}

 }}
