import {Injectable} from '@angular/core';
import {Http, Headers} from '@angular/http';
import 'rxjs/add/operator/map';
import {API_CONFIG} from '../../../../../config/API_CONFIG';
import {_getURL} from '../../../../../config/API_CONFIG';
@Injectable()
export class ProduitService{

  constructor(private http:Http){
  }
getallproduits(){
  return this.http.get(_getURL(API_CONFIG.produit))
    .map(res => res.json());

}
getallcategories(){
  return this.http.get(_getURL(API_CONFIG.produit)+'/tousCategories')
    .map(res => res.json());
}
  }
