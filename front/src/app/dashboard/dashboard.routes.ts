import { Route } from '@angular/router';
import { DashboardComponent } from './dashboard.component';
import { HomeComponent } from './home/home.component';

export const MODULE_ROUTES: Route[] =[
    {
        path: '', component: DashboardComponent,
        children: [
        { path: 'accueil', component: HomeComponent },

        ]

      },


]

export const MODULE_COMPONENTS = [
    HomeComponent,


    ]
