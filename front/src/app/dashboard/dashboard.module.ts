import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { HttpModule } from '@angular/http';
import { CommonModule } from '@angular/common';
import { MODULE_COMPONENTS, MODULE_ROUTES } from './dashboard.routes';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';

import {Ng2PaginationModule} from 'ng2-pagination';

import{SearchPipe} from './search/search.pipe';



@NgModule({
    imports: [Ng2PaginationModule,
   FormsModule,HttpModule,CommonModule,ReactiveFormsModule,RouterModule.forChild(MODULE_ROUTES)
    ],
    providers: [],
    declarations: [ MODULE_COMPONENTS,SearchPipe],
})

export class DashboardModule{}
